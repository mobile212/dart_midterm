import 'dart:io';
import 'package:stack/stack.dart';

// ผม install package stack มาใช้ครับ       $ dart pub add stack

void main(List<String> arguments) {
  print("input postfix");
  String n = stdin.readLineSync()!;

  print(evaluatePostFix(n));
}

double evaluatePostFix(String n) {
  Stack value = Stack();
  for (int i = 0; i < n.length; i++) {
    String c = n[i]; // อยากเก็บเป็น char แต่ dart ผมหา char ไม่เจอ
    if (isNumber(c)) {
      value.push(double.parse(c));
    } else {
      double right = value.pop();
      double left = value.pop();

      if (c == "+") {
        value.push(left + right);
      } else if (c == "-") {
        value.push(left - right);
      } else if (c == "*") {
        value.push(left * right);
      } else if (c == "/") {
        value.push(left / right);
      }
    }
  }
  return value.pop();
}

bool isNumber(String c) {
  return c.contains(new RegExp(r'[0-9]'));
}
