import 'dart:io';

void main(List<String> arguments) {
  print("input string");
  String n = stdin.readLineSync()!;

  print(tokenCreate(n));
}

List<String> tokenCreate(String n) {
  List<String> token = [];
  for (int i = 0; i < n.length; i++) {
    token.add(n[i]);
  }
  return token;
}
