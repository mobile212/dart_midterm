import 'package:dart_midterm/dart_midterm.dart' as dart_midterm;
import 'dart:io';
import 'package:stack/stack.dart';
// $ dart pub add stack ผม install package stack มาใช้ครับ

String? infixToPostfix(String n) {
  Stack operand = Stack();
  String postFix = "";

  for (int i = 0; i < n.length; i++) {
    String c = n[i];
    if (isNotOperand(c)) {
      postFix += c;
    } else if (c == '(') {
      operand.push('(');
    } else if (c == ')') {
      while (operand.top() != '(') {
        postFix += operand.top();
        operand.pop();
      }
      operand.pop();
    } else {
      while (operand.isNotEmpty && precedence(c) <= precedence(operand.top())) {
        postFix += operand.top();
        operand.pop();
      }
      operand.push(c);
    }
  }
  while (!operand.isEmpty) {
    postFix += operand.top();
    operand.pop();
  }
  return postFix;
}

int precedence(String c) {
  if (c == '^') {
    return 3;
  } else if (c == '/' || c == '*') {
    return 2;
  } else if (c == '+' || c == '-') {
    return 1;
  } else {
    return 0;
  }
}

bool isNotOperand(String c) {
  return c.contains(new RegExp(r'[0-9a-zA-Z]'));
}

void main(List<String> arguments) {
  String n = stdin.readLineSync()!;
  print("input infix");
  print(infixToPostfix(n));
}
